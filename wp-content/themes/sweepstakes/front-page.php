<?php get_header(); ?>

<div class="container">
    <div class="message-container">
    <div class="d-flex justify-content-center">
        <h4 class="message"></h4>
    </div>
    </div>
    
    <div class="d-flex justify-content-center">
    
    <!-- FORM START -->
        
        <form id="play-a-game" method="post" data-parsley-validate>
        <label>All fields with an * are required</label>
        <div class="form-group">
            <input type="text" class="form-control" name="full-name" id="full-name" placeholder="Full Name*" required data-parsley-pattern="^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$" data-parsley-trigger='keyup'>
        </div>
        
        <div class="form-group"  >
            <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Email Adress*" required data-parsley-type="email" data-parsley-trigger='keyup' >
        </div>
        
        <div class="form-check">
            <input class="form-check-input" name="agreement" type="checkbox" value="" id="agreement" required>
            <label class="form-check-label" for="agreement">
                *I am 18 years of age or older and I have read and agree to the <a href="">Official Rules</a>.
            </label>
            </div>
            <div class="form-check">
            <input class="form-check-input" name="newsletter" type="checkbox" value="" id="newsletter">
            <label class="form-check-label" for="newsletter">
                I would like to recive exclusive news and offers from Juicy Juice.
            </label>
            </div>
            <div class="d-flex justify-content-center g-captcha-wrapper">
                <div class="g-recaptcha" data-sitekey="6LfLNrYbAAAAAH1kFSHdXA1_XiQcc0UW1qoFey2f"></div>
            </div>
            <div class="d-flex justify-content-center button-wrapper">
                <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    <!-- FORM END -->
   

    </div>
</div>




<?php get_footer(); ?>