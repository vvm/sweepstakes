<?php
/**
 * sweepstakes functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package sweepstakes
 */

// INCLUDE SCRIPTS



function load_scripts() {
//  ADD SCRIPTS
    wp_register_script( 'jQuery', 'https://code.jquery.com/jquery-3.6.0.min.js', array('jquery'), null, true );
	wp_enqueue_script('jQuery');
    wp_register_script( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array('jquery'), null, true );
	wp_enqueue_script('slick');
	wp_register_script( 'jQuery_UI', 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js', array('jquery'), null, true );
	wp_enqueue_script('jQuery_UI');
	wp_register_script( 'jQuery_flip', 'https://cdn.rawgit.com/nnattawat/flip/master/dist/jquery.flip.min.js', array('jquery'), null, true );
	wp_enqueue_script('jQuery_flip');

	


	wp_register_script( 'bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css', array('stylesheet'), null, true );
	wp_enqueue_script('bootstrap');
	wp_enqueue_script( 'sweepstakes_script', get_stylesheet_directory_uri() . '/assets/js/script.js', array( 'jquery' ) );

	wp_enqueue_script( 'jquery_touch_punch', get_stylesheet_directory_uri() . '/js/jquery.ui.touch-punch.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'game_script', get_stylesheet_directory_uri() . '/js/lunchtime-flavorites-main.js', array( 'jquery','slick','jQuery_UI','jQuery_flip' ) );


	//PARSLEY SCRIOT
	wp_enqueue_script( 'parsley', get_stylesheet_directory_uri() . '/assets/js/parsley.min.js', array( 'jquery' ) );
	// ADD AJAX
	wp_localize_script( 'sweepstakes_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
}

add_action('wp_enqueue_scripts', 'load_scripts');


//Call Ajax from the file

add_action("wp_ajax_input_field", "input_field");
add_action("wp_ajax_nopriv_input_field", "input_field");

add_action("wp_ajax_input_field", "game_fin");
add_action("wp_ajax_nopriv_input_field", "game_fin");

function input_field() {


    include('/assets/form-submit.php');

    wp_die();

}





if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'sweepstakes_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function sweepstakes_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on sweepstakes, use a find and replace
		 * to change 'sweepstakes' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'sweepstakes', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'sweepstakes' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'sweepstakes_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'sweepstakes_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sweepstakes_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sweepstakes_content_width', 640 );
}
add_action( 'after_setup_theme', 'sweepstakes_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sweepstakes_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'sweepstakes' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'sweepstakes' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'sweepstakes_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function sweepstakes_scripts() {
	wp_enqueue_style( 'sweepstakes-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'sweepstakes-style', 'rtl', 'replace' );

	wp_enqueue_script( 'sweepstakes-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'sweepstakes_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// CUSTOM SCRIPTS 