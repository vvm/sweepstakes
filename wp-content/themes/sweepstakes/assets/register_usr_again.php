<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once ($path."/wp-content/themes/sweepstakes/assets/game_fin.php");

class Register_usr_again{
    

    public function insert_user_again($wpdb,$id,$full_name,$email,$newsletter,$date){

        $this->wpdb         = $wpdb;
        $this->id           = $id;
        $this->full_name    = $full_name;
        $this->$email       = $email;
        $this->$newsletter  = $newsletter;
        $this->date         = $date;
        $curr_date = date("Y-m-d H:i:s");


        $query_insert_user = $wpdb->prepare("
        INSERT INTO 
                    `sweeper_from_input` (`id`, `full_name`, `email`, `newsletter`, `played_a_game`, `date_registered`) 
        VALUES 
                    (NULL, '%s', '%s', '%s', '0', '%s')",
        array($full_name,$email,$newsletter,$curr_date));


        if($wpdb->query($query_insert_user)):

            $data = array(
                'status'    => 'success',
                'message'   => 'You have been inserted one more time!',
            );

            return $data;

        else:
            $data = array(
                'status'    => 'failed',
                'message'   => 'There\'s been an error during insertion into database!',
            );

            return $data;
        endif;
    }


    public function message(){


            $data = array(
                'status'    => 'success',
                'message'   => 'Thank you for playing you have been entered again!',
            );

            return $data;

    }

}

?>