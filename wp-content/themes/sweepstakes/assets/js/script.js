(function( $ ) {
    $(function() {

        $('#agreement').click(function(){
            if($(this).is(":checked")){
               $("#agreement").val('1');
            }
            else if($(this).is(":not(:checked)")){
                $("#agreement").val('0');
            }
        });

        $('#newsletter').click(function(){
            if($(this).is(":checked")){
               $("#newsletter").val('1');
            }
            else if($(this).is(":not(:checked)")){
                $("#newsletter").val('0');
            }
        });

      
        // $('#play-a-game').parsley().click('field:validated', function(e) { // Start Parsley
           
        //     var valid = $('.parsley-error').length === 0;
       

        //     if(!valid){
        //         return;
        //     }

        // });//END PARSLEY
        

        $("#play-a-game").submit(function(e) {

            e.preventDefault();
            if($('#play-a-game').parsley().isValid()){
            

            
            /**VALS*/
            // var g_captcha = $('.g-recaptcha').val();
            var full_name = $('#full-name').val();
            var email     = $('#email').val();
	        var checkbox1 = $("#agreement").val();
            var checkbox2 = $("#newsletter").val();      

           
            //FORM REGISTRATION
            $.ajax({
                type: "post",//POST METHOD
                url: '/wp-content/themes/sweepstakes/assets/form-submit.php',
                data: {
                    action   :  "input_field",
                    full_name:  full_name,
                    email    :  email,
                    checkbox2:  checkbox2,
                    g_recaptcha_response: grecaptcha.getResponse(),
                   
                },//values of all fields
                dataType: "json",//set response data type to json		
                success: function (data) {
                    console.log(JSON.stringify(data.registration_status));
                    if(data.status == 'success'){
                        // $('.message').append(data.message);
                        // $('.message').append(data.id);
                        // $('.message').append(data.full_name);
                        // $('.message').append(data.email);
                        // $('.message').append(data.date);

                        window.location.href = '/?page_id=6';
                        
                    }else if(data.status == 'failed'){
                        $('.message').append(data.message);

                    }
                    
                },
                error: function (data) {
                    console.log(JSON.stringify(data));
                    $('.message').append(data);
                }

            })
        }//ENDIF PARSLEY

        });

        

        //IF PLAYER HAS PLAYED A GAME 
        $('.done-button').click(function(){
            
            console.log("FInished the game click!");
            $.ajax({
                type: "post",//POST METHOD
                url: '/wp-content/themes/sweepstakes/assets/game_fin.php',
                data: {
                    action   :  "game_fin",
                   
                },//values of all fields
                dataType: "json",//set response data type to json		
                success: function (data) {
                    console.log(JSON.stringify(data));
                    $('.message_conf').append(data);
                
                    
                },
                error: function (data) {
                    console.log(JSON.stringify(data));
                    $('.message_conf').append(data);
                }

            })


        });


    

        

        

           
           


    });
})(jQuery);