<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
include $path.'/wp-content/themes/sweepstakes/assets/classes.php';
global $wpdb;
$validation = new Validation($wpdb);
include( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');

if(isset($_POST) && isset($_POST['g_recaptcha_response'])):
    global $wpdb;
    $captcha    = $_POST['g_recaptcha_response'];
    $full_name  = $_POST['full_name'];
    $email      = $_POST['email'];
    $newsletter = $_POST['checkbox2'];
   // CHECK IF USER EXISTS
    //var_dump($validation->check_if_user_exists_check_time($wpdb, $email)) ;
        //CAPTCHA START VALUES
        $secretKey = "6LfLNrYbAAAAAD88JuDweYsbZur5c7KgQKWzJHzu";
        
        $ip = $_SERVER['REMOTE_ADDR'];

        $url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip;
        
        $response = file_get_contents($url);

        $responseKeys = json_decode($response,true);
        //CAPTCHA END VALUES
    if(!$responseKeys['success']):
        $data = array(
            'status'    => 'failed',
            'type'      => 'captcha',
            'message'   => 'Please fill in the captcha!',
        );
        echo json_encode($data);
        die();
    else:

        if($validation->check_if_user_exists($wpdb, $email) == 'true'): //USER EXISTS
            
            if($validation->check_if_user_exists_check_time($wpdb, $email) != 'true'): //Check if user exists and if it passed 24 h from last insert
            
                $data = array(
                    'status'              => 'failed',
                    'type'                => 'max_limit',
                    'message'             => 'You can enter again tomorrow!',
                );
                echo json_encode($data);
            
            else: //if it passed 24 h user can insert again

                
            if($validation->register_new_user($wpdb,$full_name,$email,$newsletter)['status'] == 'success'):  // REGISTER USER WITH DATA FROM FORM
                $_SESSION['captcha']    =  $captcha;
                $_SESSION['timeout']    =  strtotime('+10 minutes', time());
                $_SESSION['id']         =  $validation->get_last_id_user_data($wpdb,$email)['id'];
                $_SESSION['full_name']  =  $validation->get_last_id_user_data($wpdb,$email)['full_name'];
                $_SESSION['email']      =  $validation->get_last_id_user_data($wpdb,$email)['email'];
                $_SESSION['newsletter'] =  $validation->get_last_id_user_data($wpdb,$email)['newsletter'];
                $_SESSION['date']       =  $validation->get_last_id_user_data($wpdb,$email)['date_registered'];
                
                    $data = array(
                        'status'              => 'success',
                        'message'             => 'Registration success for existing user!',
                        'id'                  => $validation->get_last_id_user_data($wpdb,$email)['id'],
                        'full_name'           => $validation->get_last_id_user_data($wpdb,$email)['full_name'],
                        'email'               => $validation->get_last_id_user_data($wpdb,$email)['email'],
                        'newsletter'          => $validation->get_last_id_user_data($wpdb,$email)['newsletter'],
                        'date'                => $validation->get_last_id_user_data($wpdb,$email)['date'],
                    );
                    echo json_encode($data);
                
            endif;
            //CHECK IF USER HAS PLAYED THE GAME IF HE DID ENTER DATA AGAIN INTO TABLE
            endif;


        else:// IF USES DO NOT EXISTS
            if($validation->register_new_user($wpdb,$full_name,$email,$newsletter)['status'] == 'success'):  // REGISTER NEW USER
                $_SESSION['captcha']    =  $captcha;
                $_SESSION['timeout']    =  strtotime('+10 minutes', time());
                $_SESSION['id']         =  $validation->get_last_id_user_data($wpdb,$email)['id'];
                $_SESSION['full_name']  =  $validation->get_last_id_user_data($wpdb,$email)['full_name'];
                $_SESSION['email']      =  $validation->get_last_id_user_data($wpdb,$email)['email'];
                $_SESSION['newsletter'] =  $validation->get_last_id_user_data($wpdb,$email)['newsletter'];
                $_SESSION['date']       =  $validation->get_last_id_user_data($wpdb,$email)['date'];
                
                    $data = array(
                        'status'              => 'success',
                        'message'             => $validation->get_last_id_user_data($wpdb,$email)['message'],
                        'id'                  => $validation->get_last_id_user_data($wpdb,$email)['id'],
                        'full_name'           => $validation->get_last_id_user_data($wpdb,$email)['full_name'],
                        'email'               => $validation->get_last_id_user_data($wpdb,$email)['email'],
                        'newsletter'          => $validation->get_last_id_user_data($wpdb,$email)['newsletter'],
                        'date'                => $validation->get_last_id_user_data($wpdb,$email)['date'],
                    );
                    echo json_encode($data);
            endif;
        endif;


    endif;//CAPTCHA END IF
endif;


?>