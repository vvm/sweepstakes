<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once ($path."/wp-content/themes/sweepstakes/assets/form-submit.php");

global $wpdb;

class Validation{
    // public $wpdb;

    // public function __construct($wpdb){
    //     $this->wpdb = $wpdb;
    // }

    


    public function check_if_user_exists($wpdb,$email){
        $this->wpdb         = $wpdb;
        $this->email        = $email;
        $date               = gmdate("Y-m-d H:i:s");
        $query              = $wpdb->prepare("SELECT *,MAX(id) AS last_id FROM  sweeper_from_input WHERE sweeper_from_input.email = '%s'",array($email));

        $results = $wpdb->get_results($query);

        foreach($results as $result):
            
           if($result->email == $email):
                return true; //USER EXISTS
           else:
                return false; //USER DO NOT EXISTS
           endif;

        endforeach;

       
    }

    public function check_if_user_exists_check_time($wpdb,$email){

        $this->wpdb         = $wpdb;
        $this->email        = $email;
        $date = gmdate("Y-m-d H:i:s");
        $query = $wpdb->prepare("SELECT *,MAX(id) AS last_id FROM  sweeper_from_input WHERE sweeper_from_input.email = '%s'",array($email));

        $results = $wpdb->get_results($query);

        if($results):
                $last_id = $results[0]->last_id;
                // $last_id    = $result->last_id;
                $get_last_user = $wpdb->prepare("SELECT * FROM sweeper_from_input WHERE id = %s",array($last_id));
                
                $results_query = $wpdb->get_results($get_last_user);

                if($results_query):
                    foreach($results_query as $last_usr):
                        
                        $id                 =  $last_usr->id;
                        $full_name          =  $last_usr->full_name;
                        $email              =  $last_usr->email;
                        $newsletter         =  $last_usr->newsletter;
                        $played_a_game      =  $last_usr->played_a_game;
                        $date_registered    =  $last_usr->date_registered;

                        //CALCULATE TIME

                        //$timediff   = strtotime($date) - strtotime($date_registered);

                        
                        $date =  strtotime($date_registered);

                        $date_registered_user = date('Y-m-d',$date);

                        $usr_can_apply_date =  date('Y-m-d', strtotime($date_registered_user. ' + 1 days'));
                        
                        $current_date = date('Y-m-d');

                        if($current_date > $date_registered_user):
                            return true;
                        else:
                            return $data = array(
                                'status' => 'false',
                                'date_enter_again' => $usr_can_apply_date,
                                'current_date'     => $current_date,
                            );
                        endif;

                    endforeach;
                endif;

                

            
        
        endif;

    }

    public function get_last_id_user_data($wpdb,$email){
        $this->wpdb         = $wpdb;
        $this->email        = $email;
        $date = gmdate("Y-m-d H:i:s");
        $query = $wpdb->prepare("SELECT *,MAX(id) AS last_id FROM  sweeper_from_input WHERE sweeper_from_input.email = '%s'",array($email));

        $results = $wpdb->get_results($query);

        if($results):


            $last_id = $results[0]->last_id;
            // $last_id    = $result->last_id;
            $get_last_user = $wpdb->prepare("SELECT * FROM sweeper_from_input WHERE id = %s",array($last_id));
            
            $results_query = $wpdb->get_results($get_last_user);

            if($results_query):
                foreach($results_query as $last_usr):
                    
                    $id                 =  $last_usr->id;
                    $full_name          =  $last_usr->full_name;
                    $email              =  $last_usr->email;
                    $newsletter         =  $last_usr->newsletter;
                    $played_a_game      =  $last_usr->played_a_game;
                    $date_registered    =  $last_usr->date_registered;
                    
                    return $data = array(
                        'status'            => 'success',
                        'message'           => 'Registration Success! Sessions Set',
                        'id'                => $id,
                        'full_name'         => $full_name,
                        'email'             => $email,
                        'newsletter'        => $newsletter,
                        'played_a_game'     => $played_a_game,
                        'date_registered'   => $date_registered,
                    );

                endforeach;

            endif;


        endif;
    }


    public function register_new_user($wpdb,$full_name,$email,$newsletter){
        $this->wpdb         = $wpdb;
        $this->full_name    = $full_name;
        $this->email        = $email;
        $this->newsletter   = $newsletter;
        $date = gmdate("Y-m-d H:i:s");

        $query = $wpdb->prepare(
            "INSERT INTO `sweeper_from_input` (`id`, `full_name`, `email`, `newsletter`, `played_a_game`, `date_registered`)
             VALUES (NULL, '%s', '%s', '%s', '0', '%s')", array($full_name,$email,$newsletter,$date));

        if($wpdb->query($query)):
        

            return $data = array(
                'status'        => 'success',
            );
        else:
            return false;
        endif;

    }



}

?>